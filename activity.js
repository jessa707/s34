// 1. Import express using require directive
const express = require('express')

// 2. Initialize express by assigning it to the app variable as a function
const app = express()

// 3. Set the port number
const port = 3000

// 4. Use middleware to allow express to read JSON
app.use(express.json())

// 5. Use middleware to allow express to be able to read more data types from a response
app.use(express.urlencoded({extended: true}))

// 6. Listen to the port and console.log a text once the server is running
app.listen(port, () => console.log(`Server is running at localhost:${port}`))

// ACTIVITY

app.get("/home", (request, response) => {
	response.send("Welcome to the home page")
})

let users = [{
    username: "johndoe",
    password: "johndoe1234"
}]

app.get("/users", (request, response) => {
    response.send(users)
})

app.delete("/delete-user", (request, response) => {
	response.send(`User ${request.body.username} has been deleted`)

})

